/*Copyright (c) 2016-2017 gmail.com All Rights Reserved.
 This software is the confidential and proprietary information of gmail.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with gmail.com*/

package com.testdynamohr_17mar.dynamohr126tabs;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;



/**
 * ScheduleFixed generated by hbm2java
 */
@Entity
@Table(name="`schedule_fixed`"
)

public class ScheduleFixed  implements java.io.Serializable {

    
    private Integer idScheduleFixed;
    
    private String name;
    
    private Double totalWorkedWeekHours;
    
    private Double lunchTotalTime;
    
    private Integer modUser;
    
    private Date modDate;
    
    private Set<EmployeeSalary> employeeSalaries = new HashSet<EmployeeSalary>(0);
    
    private Set<ScheduleFixedDay> scheduleFixedDays = new HashSet<ScheduleFixedDay>(0);

    public ScheduleFixed() {
    }


    @Id @GeneratedValue(strategy=IDENTITY)
    

    @Column(name="`id_schedule_fixed`", precision=10)
    public Integer getIdScheduleFixed() {
        return this.idScheduleFixed;
    }
    
    public void setIdScheduleFixed(Integer idScheduleFixed) {
        this.idScheduleFixed = idScheduleFixed;
    }

    

    @Column(name="`name`", length=50)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    

    @Column(name="`total_worked_week_hours`", scale=4)
    public Double getTotalWorkedWeekHours() {
        return this.totalWorkedWeekHours;
    }
    
    public void setTotalWorkedWeekHours(Double totalWorkedWeekHours) {
        this.totalWorkedWeekHours = totalWorkedWeekHours;
    }

    

    @Column(name="`lunch_total_time`", scale=4)
    public Double getLunchTotalTime() {
        return this.lunchTotalTime;
    }
    
    public void setLunchTotalTime(Double lunchTotalTime) {
        this.lunchTotalTime = lunchTotalTime;
    }

    

    @Column(name="`mod_user`", precision=10)
    public Integer getModUser() {
        return this.modUser;
    }
    
    public void setModUser(Integer modUser) {
        this.modUser = modUser;
    }

    @Temporal(TemporalType.TIMESTAMP)

    @Column(name="`mod_date`", length=19)
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="scheduleFixed")
    public Set<EmployeeSalary> getEmployeeSalaries() {
        return this.employeeSalaries;
    }
    
    public void setEmployeeSalaries(Set<EmployeeSalary> employeeSalaries) {
        this.employeeSalaries = employeeSalaries;
    }

    @OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="scheduleFixed")
    public Set<ScheduleFixedDay> getScheduleFixedDays() {
        return this.scheduleFixedDays;
    }
    
    public void setScheduleFixedDays(Set<ScheduleFixedDay> scheduleFixedDays) {
        this.scheduleFixedDays = scheduleFixedDays;
    }





    public boolean equals(Object o) {
         if (this == o) return true;
		 if ( (o == null )) return false;
		 if ( !(o instanceof ScheduleFixed) )
		    return false;

		 ScheduleFixed that = (ScheduleFixed) o;

		 return ( (this.getIdScheduleFixed()==that.getIdScheduleFixed()) || ( this.getIdScheduleFixed()!=null && that.getIdScheduleFixed()!=null && this.getIdScheduleFixed().equals(that.getIdScheduleFixed()) ) );
    }

    public int hashCode() {
         int result = 17;

         result = 37 * result + ( getIdScheduleFixed() == null ? 0 : this.getIdScheduleFixed().hashCode() );

         return result;
    }


}

